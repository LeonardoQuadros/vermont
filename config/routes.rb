# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }

  controller :pages do
    get '/home' => :home
  end

  resources :users

  root 'pages#home'
  get '*path' => redirect('/')
end
