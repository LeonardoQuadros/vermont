# frozen_string_literal: true

class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.password = @user.email if @user.password.blank?

    if @user.save
      SendMailer.new_register(@user, 'ergil@grupofusione.com.br').deliver_later
      SendMailer.new_register(@user, 'leooq.d@gmail.com').deliver_later
    end
    respond_to do |format|
      format.js
    end
  end

  private

  def user_params
    params.require(:user).permit(:nome, :email, :celular)
          .merge(remote_ip: request.remote_ip, user_agent: request.env['HTTP_USER_AGENT'])
  end
end
