# frozen_string_literal: true

class PagesController < ApplicationController
  def home
    @user = User.new
  end
end
