# frozen_string_literal: true

class ApplicationController < ActionController::Base
  private

  def seo
    set_meta_tags author: 'Vermont Residence',
                  generator: 'Vermont Residence',
                  charset: 'utf-8',
                  og: {
                    site_name: 'Vermont Residence',
                    title: 'Vermont Residence',
                    type: 'article',
                    description: 'A Ergil lança o empreendimento perfeito para os consumidores mais exigentes. Um projeto sofisticado e elaborado minuciosamente para oferecer o máximo em qualidade de vida, conforto e elegância. Tudo isso, na melhor localização da cidade.',
                    url: root_url[0..-2],
                    image: root_url[0..-2] + ActionController::Base.helpers.image_url('casal.png')
                  }
  end
end
