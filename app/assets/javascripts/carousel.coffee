$(document).on 'turbolinks:load', ->
  #close slider JS
  #------------------trent's slider

  tRunSlider = (sliderName) ->
    #slider functions

    setTextSlideHeight = ->
      if $(sliderName).hasClass('text-slider')
        if $(window).width() >= 768
          $('' + sliderName + '.text-slider').css 'height', $('' + sliderName + ' .current-t-slide .t-slide-content').height() + 245
          if $('' + sliderName + '.text-slider').hasClass('small-slider')
            $('' + sliderName + '.text-slider.small-slider').css 'height', $('' + sliderName + ' .current-t-slide .t-slide-content').height() + 100
        else
          $('' + sliderName + '.text-slider').css 'height', $('' + sliderName + ' .current-t-slide .t-slide-content').height() + 70
      return

    tStartLoadBar = ->
      $('' + sliderName + ' .t-load-bar .inner-load-bar').css 'animation', 'load 4.5s linear infinite'
      return

    tSliderHasStopped = ->
      if $('.current-t-slide').css('left') == '0px' and $('' + sliderName + ' .current-t-slide').css('right') == '0px'
        true
      else
        false

    tSlideChangerRight = ->
      if $('' + sliderName + ' .current-t-slide').next().hasClass('t-slide') and tSliderHasStopped()
        $('' + sliderName + ' .current-t-slide').removeClass('current-t-slide').css($hiddenSlideStylesLeft).next().css($tSlideInStyles).addClass 'current-t-slide'
        $('' + sliderName + ' .current-dot').removeClass('current-dot').next().addClass 'current-dot'
        setTextSlideHeight()
      else if tSliderHasStopped()
        $('' + sliderName + ' .current-t-slide').removeClass 'current-t-slide'
        $('' + sliderName + ' .t-slide').first().addClass('current-t-slide').css $tSlideInStyles
        tSetCss()
        $('' + sliderName + ' .current-dot').removeClass 'current-dot'
        $('' + sliderName + ' .t-dot').first().addClass 'current-dot'
        setTextSlideHeight()
      return

    tSlideChangerLeft = ->
      if $('' + sliderName + ' .current-t-slide').prev().hasClass('t-slide') and tSliderHasStopped()
        $('' + sliderName + ' .current-t-slide').removeClass('current-t-slide').css($hiddenSlideStylesRight).prev().css($tSlideInStyles).addClass 'current-t-slide'
        $('' + sliderName + ' .current-dot').removeClass('current-dot').prev().addClass 'current-dot'
        setTextSlideHeight()
      else if tSliderHasStopped()
        $('' + sliderName + ' .current-t-slide').removeClass 'current-t-slide'
        $('' + sliderName + ' .t-slide').last().addClass('current-t-slide').css $tSlideInStyles
        tSetCssLeft()
        $('' + sliderName + ' .current-dot').removeClass 'current-dot'
        $('' + sliderName + ' .t-dot').last().addClass 'current-dot'
        setTextSlideHeight()
      return

    tSetCss = ->
      $('' + sliderName + ' .t-slide').each (index, value) ->
        if index > 0
          $(this).css $hiddenSlideStylesRight
        return
      return

    tSetCssLeft = ->
      $t_total = $('' + sliderName + ' .t-slide').length - 1
      $('' + sliderName + ' .t-slide').each (index, value) ->
        if index < $t_total
          $(this).css $hiddenSlideStylesLeft
        return
      return

    $(sliderName).addClass 'trent-slider'
    $offset = $(sliderName).width()
    $tSlideInStyles =
      left: '0'
      right: '0'
    $t_loadBarStopStyles =
      animation: 'none'
      width: '0%'
    $hiddenSlideStylesRight =
      left: $offset
      right: 0 - $offset
    $hiddenSlideStylesLeft =
      right: $offset
      left: 0 - $offset
    #populate dots for every slide
    $('' + sliderName + ' .t-slide').each (index, value) ->
      $('' + sliderName + ' .t-slide-dots').append '<div class="t-dot"></div>'
      if index == 0
        $('' + sliderName + ' .t-dot').first().addClass 'current-dot'
      return
    #slider-code
    $tSliderHeight = $('' + sliderName + '.trent-slider').width() / 2
    if $tSliderHeight > 650
      $('.t-slide').each (index, value) ->
        $src = $('.t-slide').eq(index).find('img').attr('src')
        $slideBgStyles =
          backgroundImage: 'url(' + $src + ')'
          backgroundSize: 'cover'
          backgroudRepeat: 'no-repeat'
          backgroundPosition: 'center'
        $(this).css $slideBgStyles
        $(this).find('img').first().hide()
        return
      $tSliderHeight = 650
    $(sliderName).css 'height', $tSliderHeight
    setTextSlideHeight()
    tSetCss()
    #load bar 
    tStartLoadBar()
    #interval sllide change
    tSlideChange = window.setInterval((->
      tSlideChangerRight()
      return
    ), 4500)
    $(sliderName).mouseover(->
      $('' + sliderName + ' .t-load-bar .inner-load-bar').css $t_loadBarStopStyles
      clearInterval tSlideChange
      return
    ).mouseout ->
      $('' + sliderName + ' .t-load-bar .inner-load-bar').css $t_loadBarStopStyles
      tStartLoadBar()
      clearInterval tSlideChange
      tSlideChange = window.setInterval((->
        tSlideChangerRight()
        return
      ), 4500)
      return
    # -----slider controls
    #arrow
    $('' + sliderName + ' .t-slider-controls .arrow').click ->
      if $(this).hasClass('right-arrow')
        tSlideChangerRight()
      else if $(this).hasClass('left-arrow')
        tSlideChangerLeft()
      return
    #dots 
    $('' + sliderName + ' .t-slide-dots .t-dot').click ->
      $newDotIndex = $(this).index()
      $currentDotIndex = $('' + sliderName + ' .current-dot').index()
      if tSliderHasStopped()
        $('' + sliderName + ' .t-slide').each (index, value) ->
          $('' + sliderName + ' .current-dot').removeClass 'current-dot'
          $('' + sliderName + ' .current-t-slide').removeClass 'current-t-slide'
          $('' + sliderName + ' .t-dot').eq($newDotIndex).addClass 'current-dot'
          $('' + sliderName + ' .t-slide').eq($newDotIndex).css($tSlideInStyles).addClass 'current-t-slide'
          if index > $newDotIndex
            $(this).css $hiddenSlideStylesRight
          else if index < $newDotIndex
            $(this).css $hiddenSlideStylesLeft
          return
        setTextSlideHeight()
      return
    return

  tRunSlider '.main-slider'
  tRunSlider '.secondary-slider'
  tRunSlider '.third-slider'
  return

# ---
# generated by js2coffee 2.2.0