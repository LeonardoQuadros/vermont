$(document).on 'turbolinks:load', ->
  $('select').formSelect()
  $('.tooltipped').tooltip()
  $('.dropdown-filter-dropdown').remove()
  $('.dropdown-trigger').dropdown
    alignment: 'left'
    constrainWidth: false
    coverTrigger: false
  $('.modal').modal()
  $('.tabs').tabs()
  $('.sidenav').sidenav()
  $('.collapsible').collapsible()
  $('.datepicker').datepicker dateFormat: 'dd/mm/yy'
  $('.parallax').parallax()
  if /Mobi|Android/i.test(navigator.userAgent)
    $('.parallax-container').css('height', $(window).height()*1.6)
  else
    $('.parallax-container').css('height', $(window).height())
  if /Mobi|Android/i.test(navigator.userAgent)
    $('.parallax-container2').css('height', $(window).height()*1.7)
  else
    $('.parallax-container2').css('height', $(window).height())
  if /Mobi|Android/i.test(navigator.userAgent)
    $('.parallax-container3').css('height', $(window).height()*1.1)
  else
    $('.parallax-container3').css('height', $(window).height())
  $('#content_and_form').css('min-height', $(window).height())
  $('#modal').css('background', 'rgba(255, 255, 255, 0.8)')
  $('[data-toggle="tooltip"]').tooltip()
  M.updateTextFields();
  return

$(document).ajaxStop ->
  $('.dropdown-trigger').dropdown
  return

$(document).on 'turbolinks:before-cache', ->
  $('[data-toggle="tooltip"]').tooltip()
  $('select').formSelect('destroy')
  M.updateTextFields()
  return
