# frozen_string_literal: true

class SendMailer < ApplicationMailer
  def new_register(user, email)
    @user = user

    # @client = DeviceDetector.new(@user.user_agent)
    # @location = Location.geocode(Rails.env.development? ? '186.209.66.255' : @user.remote_ip) Fsn*2019

    mail(
      from: 'Ergil <ergil@grupofusione.com.br>',
      to: email,
      subject: 'Novo Contato', &:html
    )
  end
end
